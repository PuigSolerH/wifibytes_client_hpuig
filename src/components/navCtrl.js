/**
 * @module controllers/homeCtrl
 * @desc Contains utility functions for the contact view.
*/

import {setCookie, getCookie} from '../utils';
import {Inherence} from './inherence';

/**
 * @class MenuBarController
 * @classdesc Class representing the menu bar
 */
class MenuBarController extends Inherence {
    /**
      * @constructor
    */
    constructor(home_data,selectRule) {
        super(home_data,selectRule);
        let selectedTarget; 
        
        try{ 
            selectedTarget = document.querySelector(selectRule);
            if (selectedTarget) selectedTarget.innerHTML = this.render(home_data);
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
              }catch(e){
            if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        }; 

        // document.querySelector('#lang-es').onclick(setCookie("lang", "es"));
        // document.querySelector('#lang-en').onclick(setCookie("lang", "en"));
    }

    /** render  */
    render(home_data) {
      return `
      <header class="menu">
        <img class="menu__logo" src="${home_data.logo}">
        <nav>
            <a class='menu__link' id="home" href="#">Home</a>
            <a class='menu__link' id="contact" href="#/contact">Contact</a>
            <a class='menu__link' id="ratings" href="#/ratings">Ratings</a>
            <a class='menu__link' id="products" href="#/products">Products</a>
            <a class='menu__link' id="lang-es" href="#">ES</a>
            <a class='menu__link' id="lang-en" href="#">EN</a>
        </nav>
      </header>
      `;
    }

}

/**
 * @class FooterBarController
 * @classdesc Class used as a controller for the footer bar with Facebook and Twitter links
 */
class FooterBarController extends Inherence{

    /**
     * @constructor
     */
    constructor(data,selectRule) {
        super(data,selectRule);
        let selectedTarget; 
        
        try{ 
            selectedTarget = document.querySelector(selectRule);
            if (selectedTarget) selectedTarget.innerHTML = this.render(data);
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
              }catch(e){
            if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        };
        // try{document.querySelector(selectRule).innerHTML = this.render(data);}catch(e){console.log("error_template")};
    }
  
    /** render  */
    render(data) {
        /**
            * Here it obtains the data from the server and use it to send that to the template for filling the .html
            * @param {Array} data - Data obtained from constructor where render has been called
        */
       return `<a class='menu__link' href="#aviso_legal" id="aviso_legal">Aviso Legal</a>
       <a class='menu__link' href="#sobre_nosotros" id="sobre_nosotros">Sobre Nosotros</a>
       <a class='menu__link' href="#aviso_cookies" id="aviso_cookies">Aviso Cookies</a>
       <a id="facebook" href="${data.facebook}"><img class="menu__link--logo" src="./images/fb.png" /></a>
       <a id="twitter" href="${data.twitter}"><img class="menu__link--logo" src="./images/tw.png" /></a>`;

    }
}

export {MenuBarController, FooterBarController};