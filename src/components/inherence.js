/**
 * @module controllers/inherence
 * @desc Contains utility functions to export it in the other components.
*/

/**
 * @class Inherence
 * @classdesc Class used as a controller to export it in the other components.
 */
class Inherence {
    /**
     * @constructor
    */
    constructor(param1,param2,param3,param4) {
        try{ 
            if (!param1 && !param2 && !param3 && !param4) throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
            else {
                this.param1 = param1;
                this.param2 = param2;
                this.param3 = param3;
                this.param4 = param4;
            }
        }catch(e){
            if (param1 || param2 || param3 || param4) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        }; 
    }
}

export {Inherence};