/**
 * @module controllers/contactCtrl
 * @desc Contains utility functions for the contact view.
*/

import {Inherence} from './inherence';

/**
 * @class FooterControler
 * @classdesc Class representing the contact view
*/
class ContactControler extends Inherence {
    /**
      * @constructor
    */
    constructor(datosEmpresa,selectRule) {
      super(datosEmpresa,selectRule);
      let selectedTarget; 

      try{ 
        selectedTarget = document.querySelector(selectRule);
        if (selectedTarget) selectedTarget.innerHTML = this.render(datosEmpresa);
        else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
      }catch(e){
        if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
        throw e;
      }; 
      
      try{initMap();}catch(e){console.log("error_initMap")};

      // document.querySelector('#submit_form').addEventListener('click', submitForm);
    }
  
    /** render  */
    render(datosEmpresa) {
      /**
       * Here it obtains the data from the server and use it to send that to the template for filling the .html
       * @param {Array} datosEmpresa - Data parsed which has been obtained from server call
      */
     return `<main class="result__contact">
              <aside class="contact__title">
                  <h1>Contact</h1>
              </aside>
              <section class="result__contact contact">
                  <form class="contact__form" id="contact_form">
                      <label class="contact__label" for="name">Name</label>
                      <input class="contact__input" type="text" name="name" id="name" placeholder="Write here your name"><br/>
                      <label class="contact__label" for="email">Email</label>
                      <input class="contact__input" type="email" name="email" id="email" placeholder="Write here your email"><br/>
                      <label class="contact__label" for="reason">Reason</label>
                      <input class="contact__input" type="text" name="reason" id="reason" placeholder="Write here your reason"><br/>
                      <label class="contact__label" for="comment">Comment</label>
                      <textarea class="contact__input" id="comment" name="comment" rows="10" cols="50" placeholder="Write your opinion here"></textarea><br/>
                      <input class="contact__button" type="button" value="Send Email" id="submit_form"/>
                  </form>
                  <article>
                      <div class="contact__address" id="address">
                        <span id="phone">${datosEmpresa.phone}</span> // 
                        <span id="empresa_address">${datosEmpresa.address}</span> - 
                        <span id="city">${datosEmpresa.city}</span> - 
                        <span id="country">${datosEmpresa.country}</span>
                      </div>
                      <div class="contact__map" id="map"></div>
                  </article>
              </section>
            </main>`
    }
}

/**
  * @function initMap
  * Function for painting the map on the contact module
*/
var map;
function initMap() {
    var myLatLng = {lat: 38.90633, lng: -0.4252957};
  map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    zoom: 9
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'eCall'
  });

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      map.setCenter(pos);
    });
  }
}

/**
  * @function submitForm
  * Actually it only obtines form data when form's submit button is pressed
*/
function submitForm() {
    let hola = document.getElementById('contact_form').elements;
    console.log(hola);
    const form_data = Object.values(document.forms['contact_form'].elements).reduce((obj,field) => { obj[field.name] = field.value; return obj }, {});
    console.log(form_data);
    // get(Settings.baseURL+'/contacto/', "POST", form_data)
    // .then(function(response) {
    //     console.log(response);
    // }).catch(function(error) {
    //     console.log("Failed!", error);
    // });
}

export default ContactControler;