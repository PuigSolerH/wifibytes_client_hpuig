/**
 * @module controllers/productsCtrl
 * @desc Contains utility functions for the contact view.
*/

import {setCookie, getCookie, eraseCookie} from '../utils';
import {Inherence} from './inherence';

/**
 * @class RatingsController
 * @classdesc Class representing the ratings view inide the home module
 */
class RatingsController extends Inherence{
    /**
      * @constructor
    */
    constructor(ratingsData,selectRule) {
        super(ratingsData,selectRule);
        let selectedTarget;

        try{ 
            selectedTarget = document.querySelector(selectRule);
            if (selectedTarget) selectedTarget.innerHTML = this.render(ratingsData);
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
            if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        }; 
    }
  
    /** render  */
    render(ratingsData) {
      const destacado = ratingsData.results.map((tarifa) => {
        const subtarifa_subtarifas = tarifa.subtarifas.map((subtarifa) => {
            switch(subtarifa.tipo_tarifa) {
                case 1:
                    return `<p class="ratings__detail">${subtarifa.subtarifa_cent_minuto}cts/min</p>`;
                break;
                case 2:
                    return `<p class="ratings__detail">Land phone free calls</p>`;
                break;
                case 3:
                    return `<p class="ratings__detail">${subtarifa.subtarifa_velocidad_conexion_subida} MB/Upload & ${subtarifa.subtarifa_velocidad_conexion_bajada} MB/Download</p>`;
                break;
                case 4:
                    return `<p class="ratings__detail">${subtarifa.subtarifa_velocidad_conexion_subida} MB/Upload & ${subtarifa.subtarifa_velocidad_conexion_bajada} MB/Download </p>`;
                break;
                case 5:
                    return `<p class="ratings__detail">${subtarifa.subtarifa_num_canales} free channels</p>`;
                break;
                default:
                    return `<p class="ratings__detail"></p>`;
                break;
            }
        });
        return `
            <article class="ratings__rating">
                <div class="ratings__title">
                    <img class="ratings__img" src="${tarifa.logo}"/>
                    <label class="ratings__title ratings__text">${tarifa.nombretarifa}</label>
                </div>
                <p style="background-color:${tarifa.color.hexadecimal};" class="ratings__price">${tarifa.precio}€/mes</p>
                <div class="ratings__info">
                    ${subtarifa_subtarifas.join("")}
                </div>
                <div class="ratings ratings__buttons">
                    <input type="button" class="ratings__button" value="MORE INFO"/>
                    <input type="button" class="ratings__button" value="CONTRACTAR"/>
                </div>
            </article>
        `; 
      });
      return `<span class="content__ratings">${destacado.join("")}</span>`;
    }
  
}


/**
 * @class ProductsControler
 * @classdesc Class representing the contact view
*/
class ProductsController extends Inherence {
    /**
      * @constructor
    */
    constructor(articulosData,filtrosdata,familiadata,selectRule) {
        super(articulosData,filtrosdata,familiadata,selectRule);
        let selectedTarget = document.querySelector("#result");

        let filter = getCookie("product");

        if(!filter) {
            filter = "phon";
        }
   
        let Filtered_articulosData = articulosData.filter(articulo => articulo.codfamilia === filter);
        this.articulosData = articulosData;
        this.Filtered_articulosData = Filtered_articulosData;
        this.familiadata = familiadata;
        
        try{ 
            if (Filtered_articulosData && filtrosdata && familiadata && selectRule)
            this.render(Filtered_articulosData,filtrosdata,familiadata,selectRule);
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
            if (articulosData || filtrosdata || familiadata || selectRule) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        };

    }

    /** render  */
    render(Filtered_articulosData,filtrosdata,familiadata,selectRule) {
        // console.log(Filtered_articulosData);
      /**
       * Here it obtains the data from the server and use it to send that to the template for filling the .html
       * @param {Array} Filtered_articulosData - Data parsed which has been obtained from server call
       * @param {Array} filtrosdata - Data parsed which has been obtained from server call
       * @param {Array} familiadata - Data parsed which has been obtained from server call
       * @param {Array} selectRule - Id where the template is going to be 'painted'
      */

        let filter = getCookie("product");

        if(!filter) {
            filter = "phon";
        }

        let Filtered_familiadata = familiadata.filter(articulo => articulo.codfamilia === filter);
        this.Filtered_articulosData = this.articulosData.filter(articulo => articulo.codfamilia === filter);;

        const products = Filtered_articulosData.map((product_info) => {
            return `
            <article class="products__product">
                <img class="products__img" src="${product_info.imagen}"/>  
                <label class="products__title">${product_info.descripcion}</label>
                <p class="products__text">${product_info.pvp}€</p>
                <p class="products__text">${product_info.descripcion_breve}</p>
                <div class="products__product products__buttons">
                    <input type="button" class="products__button" value="MORE INFO"/>
                    <input type="button" class="products__button" value="CONTRACTAR"/>
                </div>
            </article>
            `;
        });

        const familias = Filtered_familiadata.map((familia) => {
                return `
                <div class="product__header" style="background-color:${familia.color.hexadecimal};">
                    <img class="product__header--img" src="${familia.imagen_cabecera}" />
                    <span class="product__header--text">
                        <p class="product__text product__text--sub">${familia.pretitulo}</p>
                        <p class="product__text product__text--main">${familia.nombre}</p>
                        <p class="product__text product__text--sub">${familia.texto_cabecera}</p>
                        <p class="product__text product__text--main">${familia.precio_cabecera}€ IVA Inc.</p>
                        <p class="product__text product__text--sub">${familia.subtexto_cabecera}</p>
                    </span>
                </div>
                `;
        });

        const familiasbtn = familiadata.map((familia) =>{
            return `<input class="filters__filter" type="button" id="${familia.codfamilia}" value="${familia.nombre}" />`;
        });

        let template =  `
            <main class="product">
                ${familias.join("")}
                <aside class="contact__title">
                    <h1>Products</h1>
                </aside>
                <article class="filters filters__content--column">
                    <p class="filters__text">Cambiar Categoria:</p>
                    <span class="filters filters__content--row">
                        ${familiasbtn.join("")}</br>
                    </span>
                    <p class="filters__text">Filtrar:</p>
                    <span class="filters filters__content--row">
                    <select class="filters__filter" id="selectMarca" name="selectMarca">
                        <option>Marca</option>  
                        <!-- TO FILL -->
                    </select>
                    <select class="filters__filter" id="selectPulgadas" name="selectPulgadas">
                        <option>Pantalla</option> 
                        <!-- TO FILL -->
                    </select>
                    <select class="filters__filter" id="selectRAM" name="selectRAM">
                        <option>Memoria RAM</option> 
                        <!-- TO FILL -->
                    </select>
                    <select class="filters__filter" id="selectProcesador" name="selectProcesador">
                        <option>Processador</option>    
                        <!-- TO FILL -->
                    </select>
                    <select class="filters__filter" id="selectCamara" name="selectCamara">
                        <option>Camara</option>   
                        <!-- TO FILL -->
                    </select>
                    </span>
                </article>
                <article class="products__content">
                    ${products.join("")}
                </article>
            </main>
        `;

        try{document.querySelector(selectRule).innerHTML = template;}catch(e){console.log("error_template")};
        
        filtrosdata.marca.forEach((item)=>{
            document.querySelector("#selectMarca").innerHTML += `<option value="${item.id}">${item.Marca}</option>`;
        }); 

        filtrosdata.pantalla.forEach((item)=>{
            document.querySelector("#selectPulgadas").innerHTML += `<option value="${item.id}">${item.num_pantalla}</option>`;
        }); 

        filtrosdata.ram.forEach((item)=>{
            document.querySelector("#selectRAM").innerHTML += `<option value="${item.id}">${item.num_ram}</option>`;
        }); 

        filtrosdata.procesador.forEach((item)=>{
            document.querySelector("#selectProcesador").innerHTML += `<option value="${item.id}">${item.num_procesador}</option>`;
        }); 

        filtrosdata.camara.forEach((item)=>{
            document.querySelector("#selectCamara").innerHTML += `<option value="${item.id}">${item.num_camara}Mp</option>`;
        });

        //Family Filter
        familiadata.forEach((family) => {
            document.getElementById(family.codfamilia).addEventListener('click', () => {
                setCookie('product', family.codfamilia);
                let filter = getCookie("product");
                let Filtered_FamiliaProducts = this.articulosData.filter(familia => familia.codfamilia === filter);
        
                this.render(Filtered_FamiliaProducts,filtrosdata,familiadata,selectRule);
            });
        });

        //Brand Filter
        document.querySelector('#selectMarca').addEventListener('change', () =>{
            let Filtered_articulosData_Filtered = this.Filtered_articulosData.filter(function (entry) { return entry.marca == document.querySelector("#selectMarca").value; });
        
            this.render(Filtered_articulosData_Filtered,filtrosdata,familiadata,selectRule);
        });
        
        //Screen Filter
        document.querySelector('#selectPulgadas').addEventListener('change', () =>{
            let Filtered_articulosData_Filtered = this.Filtered_articulosData.filter(function (entry) { return entry.marca == document.querySelector("#selectPulgadas").value; });
        
            this.render(Filtered_articulosData_Filtered,filtrosdata,familiadata,selectRule);
        });
        
        //RAM Filter
        document.querySelector('#selectRAM').addEventListener('change', () =>{
            let Filtered_articulosData_Filtered = this.Filtered_articulosData.filter(function (entry) { return entry.marca == document.querySelector("#selectRAM").value; });
                
            this.render(Filtered_articulosData_Filtered,filtrosdata,familiadata,selectRule);
        });
                
        //Processor Filter
        document.querySelector('#selectProcesador').addEventListener('change', () =>{
            let Filtered_articulosData_Filtered = this.Filtered_articulosData.filter(function (entry) { return entry.marca == document.querySelector("#selectProcesador").value; });
                
            this.render(Filtered_articulosData_Filtered,filtrosdata,familiadata,selectRule);
        });
        
        //Camera Filter
        document.querySelector('#selectCamara').addEventListener('change', () => {
            let Filtered_articulosData_Filtered = this.Filtered_articulosData.filter(function (entry) { return entry.marca == document.querySelector("#selectCamara").value; });
        
            this.render(Filtered_articulosData_Filtered,filtrosdata,familiadata,selectRule);
        });

    }
}

export {RatingsController, ProductsController};