/**
 * @module controllers/footerCtrl
 * @desc Contains utility functions for the policies views whose links are on the footer.
*/

import {Inherence} from './inherence';

/**
 * @class FooterControler
 * @classdesc Class used as a controller for the footer policies
 */
class FooterControler extends Inherence {
    
    /**
     * @constructor
     */
    constructor(data,selectRule,section) {
        super(data,selectRule,section);
        let selectedTarget; 

        try{ 
			selectedTarget = document.querySelector(selectRule);
			if (selectedTarget) selectedTarget.innerHTML = this.render(data,section);
			else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
			if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
			throw e;
        }; 
    }
  
    /** render  */
    render(data,section) {
        /**
            * Here it obtains the data from the server and use it to send that to the template for filling the .html
            * @param {String} section - Depending of the section the render will return one template or another one.
            * @param {Array} data - Data parsed which has been obtained from server call and then filtered to obtain only the footer data.
        */
       if (section === 'aviso_legal') {
            return `
            <article>
                <h1 class="footer__title">Aviso Legal</h1>
                <p>${data[1].content}</p>
            </article>
            `;
        } else if (section === 'sobre_nosotros') {
            return `
            <article>
                <h1 class="footer__title">Sobre Nosotros</h1>
                <img class="footer__logo" src="${data[4].image}"/>
                <p>${data[4].content}</p>
            <article>
            `; 
        } else if (section === 'aviso_cookies') {
            return `
            <article>
                <h1 class="footer__title">Aviso Cookies</h1>
                <p>${data[3].content}</p>
            </article>
            `;
        } else {
            return `<h1 class="footer__title">PAGE NOT FOUND</h1>`;
        }

    }
}

export default FooterControler;