/**
 * @module controllers/homeCtrl
 * @desc Contains utility functions for the contact view.
*/

import {Inherence} from './inherence';

/**
 * @class HomeController
 * @classdesc Class representing the home view
 */
class HomeController extends Inherence {
    /**
      * @constructor
    */
    constructor(datosHome,selectRule) {
      super(datosHome,selectRule);
      let selectedTarget;  

      try{ 
        selectedTarget = document.querySelector(selectRule);
        if (selectedTarget) selectedTarget.innerHTML = this.render(datosHome);
        else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
          }catch(e){
        if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
        throw e;
      }; 
    }

    /** render  */
    render(datosHome) {
      return `
        <main class="result__content">
            <aside class="content__title">
                <h1>${datosHome[0].titulo}</h1>
                <h2>${datosHome[0].subtitulo}</h2>
            </aside>
            <aside class="result__slider" id="slider"></aside>
            <h2 class="content__title">Nuestras tarifas</h2>
            <section id="tarifas"></section>
            <aside id="video" class="result__video"></aside>
        </main>
        <aside class="result__about">
        <div class="about__info">
            <label class="about__title">${datosHome[0].caja_izquierda_titulo}</label>
            <p>${datosHome[0].caja_izquierda_texto}</p>
        </div>
        <div class="about__info">
            <label class="about__title">${datosHome[0].caja_derecha_titulo}</label>
            <p>${datosHome[0].caja_derecha_texto}</p>
        </div>
        </aside>
      `
    }

}

/**
 * @class VideoController
 * @classdesc Class representing the video view inide the home module
 */
class VideoController extends Inherence {

  constructor(videoData,selectRule) {
    super(videoData,selectRule);
    let selectedTarget;  

    try{ 
      selectedTarget = document.querySelector(selectRule);
      if (selectedTarget) selectedTarget.innerHTML = this.render(videoData);
      else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
      if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
      throw e;
    }; 
  }

  render(videoData) {
    return `
    <video src="${videoData[0].image}" controls>
        Tu navegador no implementa el elemento <code>video</code>.
    </video>
    `;
  }

}

/**
 * @class SliderController
 * @classdesc Class representing the slider view inide the home module
 */
class SliderController extends Inherence {

  constructor(slider_data,selectRule) {
    super(slider_data,selectRule);
    let selectedTarget;  

    try{ 
      selectedTarget = document.querySelector(selectRule);
      if (selectedTarget) selectedTarget.innerHTML = this.render(slider_data);
      else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
      if (selectedTarget) selectedTarget.innerHTML =" Problems rendering "+this.constructor.name+" -> "+e;
      throw e;
    }; 
  }

  render(slider_data) {
    const slide = slider_data.map((slider) => {
      return `
      <div class="slider__slide"><p class="slider__text">${slider.content}</p><img src="${slider.image}" /></div>
      `;
    });
    return `
    <div class="slider__slideshow" id="slideshow">
      <div class="slider__slide-wrapper">
          ${slide.join("")}
      </div>
    </div>
    `;
  }

}

export {HomeController, VideoController ,SliderController};