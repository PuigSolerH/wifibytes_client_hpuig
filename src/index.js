/** importing functions*/
import {Router} from './router.js'; //Knows what to do for every single URL 
import {HomeController, SliderController, VideoController} from './components/homeCtrl';
import ContactControler from './components/contactCtrl';
import FooterControler from './components/footerCtrl';
import {RatingsController, ProductsController} from './components/productsCtrl';
import {MenuBarController, FooterBarController} from './components/navCtrl';
import {get} from './utils';

/** redirect to each view using routing function */
Router
.add(/contact/, function() {
  console.log("Contact");
  /** obtaining data from server for filling the view in the controller */
  get('/datos_empresa').then(function(response) {         
    let datosEmpresa = JSON.parse(response);

    new ContactControler(datosEmpresa,"#result");

  }).catch(function(error) {
    console.log("Failed!", error);
  });
}).listen()
.add(/aviso_legal/, function() {
  console.log("aviso legal");
    /** obtaining data from server for filling the view in the controller */
  get('/datos_empresa').then(function(response) {         
    let data = JSON.parse(response);
    data = data.textos.filter(function(entry) {return /footer/.test(entry.key); });

    new FooterControler(data,"#result","aviso_legal");

  }).catch(function(error) {
    console.log("Failed!", error);
  });
}).listen()
.add(/sobre_nosotros/, function() {
  console.log("sobre nosotros");
    /** obtaining data from server for filling the view in the controller */
  get('/datos_empresa').then(function(response) {         
    let data = JSON.parse(response);
    data = data.textos.filter(function(entry) {return /footer/.test(entry.key); });

    new FooterControler(data,"#result","sobre_nosotros");

  }).catch(function(error) {
    console.log("Failed!", error);
  });
}).listen()
.add(/aviso_cookies/, function() {
  console.log("avio cookies");
    /** obtaining data from server for filling the view in the controller */
  get('/datos_empresa').then(function(response) {         
    let data = JSON.parse(response);
    data = data.textos.filter(function(entry) {return /footer/.test(entry.key); });

    new FooterControler(data,"#result","aviso_cookies");

  }).catch(function(error) {
    console.log("Failed!", error);
  });
}).listen()
.add(/ratings/, function() {
  console.log("Ratings");
  /** obtaining data from server for filling the view in the controller */
  get('/tarifa?activo=true').then(function(response) {           
    let ratingsData=JSON.parse(response);

    new RatingsController(ratingsData,"#result");

  }).catch(function(error) {
    console.log("Failed!", error);
  });
}).listen()
.add(/products/, function() {
  console.log("Products");
  /** obtaining data from server for filling the view in the controller */
  get('/articulo?activo=true').then(function(response) {           
    let articulosData=JSON.parse(response);

    get('/filtros').then(function(response) {           
      let filtrosdata=JSON.parse(response);
  
      get('/familia').then(function(response) {           
        let familiadata=JSON.parse(response);
    
        new ProductsController(articulosData.results,filtrosdata,familiadata.results,"#result");
      });
    }); 
  });
}).listen()
// .add(/products\/(.*)\/edit\/(.*)/, function() {
//   console.log('products', arguments);
// })
.add(function() {
  console.log('default');
  /** obtaining data from server for filling the view in the controller */
  get('/home').then(function(response) {           
    let datosHome=JSON.parse(response);

    new HomeController(datosHome,"#result");

  }).catch(function(error) {
    console.log("Failed!", error);
  });

  /** obtaining data from server for filling the view in the controller */
  get('/tarifa?destacado=true').then(function(response) {           
    let ratingsData=JSON.parse(response);

    new RatingsController(ratingsData,"#tarifas");
    
  }).catch(function(error) {
    console.log("Failed!", error);
  });

  /** obtaining data from server for filling the view in the controller */
  get('/datos_empresa').then(function(response) {           
    let data=JSON.parse(response);
    let slider_data = data.textos.filter(function(entry) {return /jumbotron/.test(entry.key);});
    let videoData = data.textos.filter(function(entry) {return /video/.test(entry.key);});
    
    new SliderController(slider_data,"#slider");
    new VideoController(videoData,"#video");

  }).catch(function(error) {
    console.log("Failed!", error);
  });

});

/** * DOMContentLoaded fires when the HTML and scripts that make up the page have loaded, but not necessarily the images or CSS. */
document.addEventListener("DOMContentLoaded", function() {
  // this function runs when the DOM is ready, i.e. when the document has been parsed
  Router.navigate('home');
  get('/datos_empresa', 'GET').then(function(response) {
    let data = JSON.parse(response);

    new FooterBarController(data,"#footer");
    new MenuBarController(data,'#header');
  }); 

});
