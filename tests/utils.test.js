import {get, getCookie, setCookie, eraseCookie} from '../src/utils';

const mockXHR = {
  open: jest.fn(),
  onload: jest.fn(),
  onerror: jest.fn(),
  send: jest.fn(),
  readyState: 4,
  status: 200,
  statusText: JSON.stringify(
      [
          { title: 'test post' },
          { tile: 'second test post' }
      ]
  )
};

window.XMLHttpRequest = jest.fn(() => mockXHR);

test('Should retrieve the list of posts from the server when calling getPosts method', () => {
  const reqPromise = get();
  mockXHR.onload();
  reqPromise.then((posts) => {
      expect(posts.length).toBe(2);
      expect(posts[0].title).toBe('test post');
      expect(posts[1].title).toBe('second test post');
      done();
  });
});

// it('Should retrieve a server error when calling get', function(done) {
//     mockXHR.status=400; //Simulate error
//     window.XMLHttpRequest = jest.fn(() => mockXHR);
//     const reqPromise =get('posts');
//     mockXHR.onload();
//     reqPromise.then((posts) => {
//         console.log(posts);
//         posts=JSON.parse(posts);
//         done();
//     });
// });

it('Get and set Cookies works', () => {
    setCookie("hola", "mundo");
    let info = getCookie("hola");
    expect(info).toBe("mundo");    
});

it('Remove Cookies works', () => {
    setCookie("hola", "mundo");
    eraseCookie("hola");
    let info = getCookie("hola");
    expect(info).toBe(null);    
});