const $ = require('jquery');
import FooterControler from '../src/components/footerCtrl';
import {datos_empresa} from './data/datos_empresa';
beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta property="og:type" content="shop" />
            <meta property="og:title" content="eCall" />
            <meta property="og:url" content="http://localhost:8080" />
            <meta property="og:image" content="http://carretaderecetas.com/wp-content/uploads/2014/08/el-cafe.jpg" />
            <meta property="og:description" content="This application is made for educational purposes"/>
            <meta property="og:site_name" content="eCall" />
            <meta property="og:locale" content="es_ES" />
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="css/styles.css">
            <script src="main.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMPiLJ8N9V80daGmjDL_xS8WUjcbfCXpI&callback=initMap" async defer></script>
        </head>
        <body>
            <header id="header" class="menu"></header>
            <div id="result"></div>
            <footer id="footer" class="menu"></footer>
        </body>
    </html>`;
  fakeDOMLoaded();
});

let data = datos_empresa;
data = data.textos.filter(function(entry) {return /footer/.test(entry.key); });

it('We can check if Contact component called the class constructor', () => {
  const contactIns = new FooterControler(data, "#result", "aviso_legal"); 
  expect(contactIns.constructor.name).toBe('FooterControler');
});

it('FooterControler render must be called and it works properly', () => {
  new FooterControler(data, "#result", "aviso_legal"); 
  expect($('#result').children.length).toBeGreaterThan(1);    
});

it('Component must fail due to target html tag to render in doesnt exist', () => {
  expect(function(){new FooterControler(data, "#resultt", "aviso_legal")}).toThrowError(/Error/i);    
});

it('Component must fail due to JSON input doesnt contains expected information', () => { 
  expect(function(){new FooterControler(undefined, "#result", "aviso_legal")}).toThrowError(/undefined/);    
});

// it('Component must fail due to section not exists', () => { 
//   expect(function(){new FooterControler(data, "#result", undefined)}).toThrowError(/undefined/);    
// });

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}