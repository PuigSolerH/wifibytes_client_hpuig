const $ = require('jquery');
import {RatingsController, ProductsController} from '../src/components/productsCtrl';
import {articulo} from './data/articulo';
import {familia} from './data/familia';
import {filtros} from './data/filtros';
import {tarifa} from './data/tarifa';

beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta property="og:type" content="shop" />
            <meta property="og:title" content="eCall" />
            <meta property="og:url" content="http://localhost:8080" />
            <meta property="og:image" content="http://carretaderecetas.com/wp-content/uploads/2014/08/el-cafe.jpg" />
            <meta property="og:description" content="This application is made for educational purposes"/>
            <meta property="og:site_name" content="eCall" />
            <meta property="og:locale" content="es_ES" />
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="css/styles.css">
            <script src="main.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMPiLJ8N9V80daGmjDL_xS8WUjcbfCXpI&callback=initMap" async defer></script>
        </head>
        <body>
          <header id="header" class="menu"></header>
          <div id="result"></div>
          <footer id="footer" class="menu"></footer>
        </body>
    </html>`;
  fakeDOMLoaded();
});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

//RATINGS
it('We can check if Ratings component called the class constructor', () => {
  const contactIns = new RatingsController(tarifa, "#result"); 
  expect(contactIns.constructor.name).toBe('RatingsController');
});

it('RatingsController render must be called and it works properly', () => {
  new RatingsController(tarifa, "#result"); 
  expect($('#result').children.length).toBeGreaterThan(1);    
});

it('Component must fail due to target html tag to render in doesnt exist', () => { 
  expect(function(){new RatingsController(tarifa, "#resultt")}).toThrowError(/Error/i);    
});

it('Component must fail due to JSON input doesnt contains expected information', () => { 
  expect(function(){new RatingsController(undefined, "#result")}).toThrowError(/undefined/);    
});

//PRODUCTS
it('We can check if Products component called the class constructor', () => {
  const contactIns = new ProductsController(articulo.results,filtros,familia.results,"#result"); 
  expect(contactIns.constructor.name).toBe('ProductsController');
});
    
it('ProductsController render must be called and it works properly', () => {
  new ProductsController(articulo.results,filtros,familia.results,"#result"); 
  expect($('#result').children.length).toBeGreaterThan(1);    
});
    
// it('Component must fail due to target html tag to render in doesnt exist', () => { 
//   expect(function(){new ProductsController(articulo.results,filtros,familia.results,"#resultt")}).toThrowError(/Error/i);    
// });
    
// it('Component must fail due to JSON input doesnt contains expected information', () => { 
//   expect(function(){new ProductsController(undefined, undefined, undefined, undefined, "#result")}).toThrowError(/Error/i);    
// });