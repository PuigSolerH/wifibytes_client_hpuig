let tarifa = {
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "codtarifa": 1,
            "nombretarifa": "Podium",
            "slug": "podium",
            "pretitulo": "Tarifas Móviles",
            "logo": "http://127.0.0.1:8000/media/Logo/bronce.jpg",
            "precio": 6.95,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 1,
                "titulo": "Bronce",
                "hexadecimal": "#cd7f32"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 1,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": 8.0,
                    "subtarifa_est_llamada": 5.0,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": null,
                    "subtarifa_velocidad_conexion_bajada": null,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "MOVIL_PODIUM",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 1,
                        "nombretarifa": "Podium",
                        "slug": "podium",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/bronce.jpg",
                        "precio": 6.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540929943,
                        "updated_at": 1541616738,
                        "color": 1
                    }
                },
                {
                    "subtarifa_id": 2,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 4.0,
                    "subtarifa_velocidad_conexion_bajada": 4.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "WIFI_PODIUM",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 4,
                    "subtarifa_tarifa": {
                        "codtarifa": 1,
                        "nombretarifa": "Podium",
                        "slug": "podium",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/bronce.jpg",
                        "precio": 6.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540929943,
                        "updated_at": 1541616738,
                        "color": 1
                    }
                }
            ]
        },
        {
            "codtarifa": 2,
            "nombretarifa": "Sub-Champion",
            "slug": "sub-champion",
            "pretitulo": "Tarifas Móviles",
            "logo": "http://127.0.0.1:8000/media/Logo/plata.jpg",
            "precio": 11.95,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 2,
                "titulo": "Silver",
                "hexadecimal": "#C0C0C0"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 3,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": 2.0,
                    "subtarifa_est_llamada": 0.0,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": null,
                    "subtarifa_velocidad_conexion_bajada": null,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "MOVIL_SUB",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 2,
                        "nombretarifa": "Sub-Champion",
                        "slug": "sub-champion",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/plata.jpg",
                        "precio": 11.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540930024,
                        "updated_at": 1541616805,
                        "color": 2
                    }
                },
                {
                    "subtarifa_id": 4,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 10.0,
                    "subtarifa_velocidad_conexion_bajada": 10.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "FIBRA_SUB",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 3,
                    "subtarifa_tarifa": {
                        "codtarifa": 2,
                        "nombretarifa": "Sub-Champion",
                        "slug": "sub-champion",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/plata.jpg",
                        "precio": 11.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540930024,
                        "updated_at": 1541616805,
                        "color": 2
                    }
                }
            ]
        },
        {
            "codtarifa": 3,
            "nombretarifa": "Champion",
            "slug": "champion",
            "pretitulo": "Tarifas Móviles",
            "logo": "http://127.0.0.1:8000/media/Logo/oro.jpg",
            "precio": 19.95,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 3,
                "titulo": "Gold",
                "hexadecimal": "#FFD700"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 5,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 30.0,
                    "subtarifa_velocidad_conexion_bajada": 30.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "FIBRA_CHAMPION",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 3,
                    "subtarifa_tarifa": {
                        "codtarifa": 3,
                        "nombretarifa": "Champion",
                        "slug": "champion",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/oro.jpg",
                        "precio": 19.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540930098,
                        "updated_at": 1541616878,
                        "color": 3
                    }
                },
                {
                    "subtarifa_id": 6,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": null,
                    "subtarifa_velocidad_conexion_bajada": null,
                    "subtarifa_num_canales": 50,
                    "subtarifa_siglas_omv": "TV_CHAMPION",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 5,
                    "subtarifa_tarifa": {
                        "codtarifa": 3,
                        "nombretarifa": "Champion",
                        "slug": "champion",
                        "pretitulo": "Tarifas Móviles",
                        "pretitulo_va": "Tarifes Mòbils",
                        "logo": "media/Logo/oro.jpg",
                        "precio": 19.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540930098,
                        "updated_at": 1541616878,
                        "color": 3
                    }
                }
            ]
        },
        {
            "codtarifa": 5,
            "nombretarifa": "Beginner",
            "slug": "beginner",
            "pretitulo": "Tarifas Móviles",
            "logo": "http://127.0.0.1:8000/media/Logo/novel.jpg",
            "precio": 0.0,
            "activo": true,
            "destacado": false,
            "color": {
                "id": 5,
                "titulo": "Verde",
                "hexadecimal": "#81be4d"
            },
            "subtarifas": []
        }
    ]
}

export {tarifa};