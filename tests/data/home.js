let home = [
    {
        "pk": 1,
        "titulo": "Wifibytes",
        "subtitulo": "Otra forma de hacer comunicación",
        "caja_izquierda_titulo": "¿Por qué Wifibytes?",
        "caja_izquierda_texto": "<p dir=\"ltr\"><span>&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos asistidos.</span></p>\r\n<p>&nbsp;</p>\r\n<p dir=\"ltr\"><span>Simple y facil. Todo el soporte y atenci&oacute;n v&iacute;a internet con un sistema de tickets.</span></p>\r\n<p>&nbsp;</p>",
        "caja_derecha_titulo": "¿Por qué Wifibytes?",
        "caja_derecha_texto": "<p><span>&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos </span></p>\r\n<p>&nbsp;</p>",
        "activo": true,
        "idioma": 1,
        "lang": "es"
    },
    {
        "pk": 2,
        "titulo": "Wifibytes",
        "subtitulo": "Altra forma de fer communicació",
        "caja_izquierda_titulo": "Per qué Wifibytes?",
        "caja_izquierda_texto": "<pre class=\"prettyprint\"><span lang=\"ca\">Vols donar-te de baixa? Canviar de tarifa? Activar el roaming? &iquest;Desactivar l'b&uacute;stia de veu? Sense problema. Entra en el teu compte i fes-ho amb un sol click.Sin preguntes o trucades a tel&egrave;fons assistits.\r\n\r\n&nbsp;\r\n\r\nSimple i f&agrave;cil. Tot el suport i atenci&oacute; via internet amb un sistema de tiquets.</span></pre>",
        "caja_derecha_titulo": "Per qué Wifibytes?",
        "caja_derecha_texto": "<pre id=\"tw-target-text\" class=\"tw-data-text tw-ta tw-text-small\" style=\"text-align: left; height: 144px;\" dir=\"ltr\" data-placeholder=\"Traducci&oacute;n\" data-fulltext=\"\"><span lang=\"ca\">Vols donar-te de baixa? Canviar de tarifa? Activar el roaming? &iquest;Desactivar l'b&uacute;stia de veu? Sense problema. Entra en el teu compte i fes-ho amb un sol click.Sin preguntes o trucades a tel&egrave;fons</span></pre>",
        "activo": true,
        "idioma": 2,
        "lang": "va"
    }
]

export {home};