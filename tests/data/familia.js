let familia = {
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "codfamilia": "phon",
            "slug": "smartphones",
            "nombre": "Smartphones",
            "color": {
                "id": 4,
                "titulo": "Red",
                "hexadecimal": "#d43c57"
            },
            "icono": "http://127.0.0.1:8000/media/familia/smartphone.jpg",
            "pretitulo": "Lo último en",
            "titulo": "Smartphones",
            "precio_cabecera": 550.0,
            "imagen_cabecera": "http://127.0.0.1:8000/media/familia/oneplus6t.jpg",
            "thumbnail": null,
            "texto_cabecera": "<pre class=\"prettyprint\"><span class=\"str\">Los mejores terminales del mercado desde</span></pre>",
            "subtexto_cabecera": "<pre class=\"prettyprint\"><span class=\"str\">No olvides que todos nuestros terminales son libres y que no tienes permanencia al contratar nuestras tarifas.</span></pre>"
        },
        {
            "codfamilia": "sim",
            "slug": "tarjetas",
            "nombre": "Tarjetas",
            "color": {
                "id": 5,
                "titulo": "Verde",
                "hexadecimal": "#81be4d"
            },
            "icono": "http://127.0.0.1:8000/media/familia/sim.jpg",
            "pretitulo": "Tipos tarjetas Sim",
            "titulo": "Tarjeta",
            "precio_cabecera": 0.0,
            "imagen_cabecera": "http://127.0.0.1:8000/media/familia/sims.jpg",
            "thumbnail": null,
            "texto_cabecera": "<pre class=\"prettyprint\"><span class=\"str\">Tarjetas</span></pre>",
            "subtexto_cabecera": "<pre class=\"prettyprint\"><span class=\"str\">Tarjetas</span></pre>"
        }
    ]
}

export {familia};