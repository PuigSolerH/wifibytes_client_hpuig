let filtros = {
    "pantalla": [
        {
            "id": 1,
            "num_pantalla": 5.8
        },
        {
            "id": 2,
            "num_pantalla": 6.41
        }
    ],
    "procesador": [
        {
            "id": 1,
            "num_procesador": "Apple A11"
        },
        {
            "id": 2,
            "num_procesador": "Qualcomm Snapdragon 845"
        }
    ],
    "ram": [
        {
            "id": 1,
            "num_ram": "3 GB"
        },
        {
            "id": 2,
            "num_ram": "8 GB"
        }
    ],
    "marca": [
        {
            "id": 1,
            "Marca": "Apple"
        },
        {
            "id": 2,
            "Marca": "OnePlus"
        },
        {
            "id": 3,
            "Marca": "Yoigo"
        }
    ],
    "camara": [
        {
            "id": 1,
            "num_camara": 2.0
        },
        {
            "id": 2,
            "num_camara": 3.2
        }
    ]
}

export {filtros};