const $ = require('jquery');
import {Inherence} from '../src/components/inherence';
import {datos_empresa} from './data/datos_empresa';
import {articulo} from './data/articulo';
import {familia} from './data/familia';
import {filtros} from './data/filtros';
import {home} from './data/home';
import {tarifa} from './data/tarifa';

beforeEach(() => {
    // Set up our document body
    document.body.innerHTML = `
      <!DOCTYPE html>
      <html lang="en">
          <head>
              <meta charset="utf-8">
              <meta property="og:type" content="shop" />
              <meta property="og:title" content="eCall" />
              <meta property="og:url" content="http://localhost:8080" />
              <meta property="og:image" content="http://carretaderecetas.com/wp-content/uploads/2014/08/el-cafe.jpg" />
              <meta property="og:description" content="This application is made for educational purposes"/>
              <meta property="og:site_name" content="eCall" />
              <meta property="og:locale" content="es_ES" />
              <title>title</title>
              <link rel="stylesheet" type="text/css" href="css/styles.css">
              <script src="main.js"></script>
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMPiLJ8N9V80daGmjDL_xS8WUjcbfCXpI&callback=initMap" async defer></script>
          </head>
          <body>
            <header id="header" class="menu"></header>
            <div id="result"></div>
            <footer id="footer" class="menu"></footer>
          </body>
      </html>`;
    fakeDOMLoaded();
});
  
function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');
    
    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
}

it('We can check if Inherence component called the class constructor', () => {
    const contactIns = new Inherence(datos_empresa, articulo, familia, "#result"); 
    expect(contactIns.constructor.name).toBe('Inherence');
});
  
it('Inherence render must be called and it works properly', () => {
    new Inherence(datos_empresa, articulo, familia, "#result"); 
    expect($('#result').children.length).toBeGreaterThan(1);    
});
  
// it('Component must fail due to target html tag to render in doesnt exist', () => {
//     expect(function(){new Inherence(datos_empresa, articulo, familia, filtros, "#resultt")}).toThrowError(/Error/i);    
// });
  
// it('Component must fail due to JSON input doesnt contains expected information', () => { 
//     expect(function(){new Inherence(undefined, undefined, undefined, "#result")}).toThrowError(/undefined/);    
// }); 