
const index=require('../src/index.js');
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =
    `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta property="og:type" content="shop" />
        <meta property="og:title" content="eCall" />
        <meta property="og:url" content="http://localhost:8080" />
        <meta property="og:image" content="http://carretaderecetas.com/wp-content/uploads/2014/08/el-cafe.jpg" />
        <meta property="og:description" content="This application is made for educational purposes"/>
        <meta property="og:site_name" content="eCall" />
        <meta property="og:locale" content="es_ES" />
        <title>title</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <script src="main.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMPiLJ8N9V80daGmjDL_xS8WUjcbfCXpI&callback=initMap" async defer></script>
    
      </head>
      <body>
        <header id="header" class="menu"></header>
        <div id="result"></div>
        <footer id="footer" class="menu"></footer>
      </body>
    </html>
    `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();
  
});

test('First test select index load header', () => {
  //console.log("$('footer').children.length=>"+$('footer').children.length);
  expect($('header').children.length).toBeGreaterThan(1);    
});

test('Second test result filled', () => {
  //console.log("$('nav').children.length=>"+$('nav').children.length);
  expect($('result').children.length).toBeGreaterThan(1);    
});

test('Third footer section filled', () => {
  //console.log("$('#main').children.length=>"+$('main').children.length);
  expect($('footer').children.length).toBeGreaterThan(1);    
});
