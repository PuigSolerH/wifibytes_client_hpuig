const $ = require('jquery');
import {HomeController, VideoController ,SliderController} from '../src/components/homeCtrl';
import {datos_empresa} from './data/datos_empresa';
import {home} from './data/home';

beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta property="og:type" content="shop" />
            <meta property="og:title" content="eCall" />
            <meta property="og:url" content="http://localhost:8080" />
            <meta property="og:image" content="http://carretaderecetas.com/wp-content/uploads/2014/08/el-cafe.jpg" />
            <meta property="og:description" content="This application is made for educational purposes"/>
            <meta property="og:site_name" content="eCall" />
            <meta property="og:locale" content="es_ES" />
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="css/styles.css">
            <script src="main.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMPiLJ8N9V80daGmjDL_xS8WUjcbfCXpI&callback=initMap" async defer></script>
        </head>
        <body>
          <header id="header" class="menu"></header>
          <div id="result"></div>
          <footer id="footer" class="menu"></footer>
        </body>
    </html>`;
  fakeDOMLoaded();
});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

//HomeController
it('We can check if Contact component called the class constructor', () => {
  const contactIns = new HomeController(home, "#result"); 
  expect(contactIns.constructor.name).toBe('HomeController');
});

it('HomeController render must be called and it works properly', () => {
  new HomeController(home, "#result"); 
  expect($('#result').children.length).toBeGreaterThan(1);    
});

it('Component must fail due to target html tag to render in doesnt exist', () => {
  expect(function(){new HomeController(home, "#resultt")}).toThrowError(/Error/i);    
});

it('Component must fail due to JSON input doesnt contains expected information', () => { 
  expect(function(){new HomeController(undefined, "#result")}).toThrowError(/undefined/);    
}); 

//VideoController
let video_data = datos_empresa.textos.filter(function(entry) {return /video/.test(entry.key);});

it('We can check if Contact component called the class constructor', () => {
    const contactIns = new VideoController(video_data, "#result"); 
    expect(contactIns.constructor.name).toBe('VideoController');
});
  
it('VideoController render must be called and it works properly', () => {
    new VideoController(video_data, "#result"); 
    expect($('#result').children.length).toBeGreaterThan(1);    
});
  
it('Component must fail due to target html tag to render in doesnt exist', () => {
    expect(function(){new VideoController(video_data, "#resultt")}).toThrowError(/Error/i);    
});
  
it('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new VideoController(undefined, "#result")}).toThrowError(/undefined/);    
});

//SliderController
let slider_data = datos_empresa.textos.filter(function(entry) {return /jumbotron/.test(entry.key);});

it('We can check if Contact component called the class constructor', () => {
    const contactIns = new SliderController(slider_data, "#result"); 
    expect(contactIns.constructor.name).toBe('SliderController');
});
  
it('SliderController render must be called and it works properly', () => {
    new SliderController(slider_data, "#result"); 
    expect($('#result').children.length).toBeGreaterThan(1);    
});
  
it('Component must fail due to target html tag to render in doesnt exist', () => {
    expect(function(){new SliderController(slider_data, "#resultt")}).toThrowError(/Error/i);    
});
  
it('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new SliderController(undefined, "#result")}).toThrowError(/undefined/);    
}); 